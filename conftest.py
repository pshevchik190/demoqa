import allure
import pytest

from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener


@pytest.fixture
def driver(status='lab'):
    options = Options()
    options.set_capability('unhandledPromptBehavior', 'accept')
    options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
    options.add_argument('--headless')
    options.add_argument('--window-size=1920x1080')
    if status == 'Chrome':
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        driver.maximize_window()
    else:
        driver = webdriver.Remote(command_executor="http://selenium__standalone-chrome:4444/wd/hub", options=options)
        driver.maximize_window()
    yield driver
    attach = driver.get_screenshot_as_png()
    allure.attach(attach, name=f"Screenshot {datetime.today()}", attachment_type=allure.attachment_type.PNG)
    driver.quit()
